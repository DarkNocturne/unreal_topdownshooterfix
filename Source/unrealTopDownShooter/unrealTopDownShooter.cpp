// Copyright Epic Games, Inc. All Rights Reserved.

#include "unrealTopDownShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, unrealTopDownShooter, "unrealTopDownShooter" );

DEFINE_LOG_CATEGORY(LogunrealTopDownShooter)
 