// Copyright Epic Games, Inc. All Rights Reserved.

#include "unrealTopDownShooterGameMode.h"
#include "unrealTopDownShooterPlayerController.h"
#include "unrealTopDownShooter/Character/unrealTopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

AunrealTopDownShooterGameMode::AunrealTopDownShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AunrealTopDownShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/_MainProject/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;

	}
}

void AunrealTopDownShooterGameMode::PlayerCharacterDead()
{

}