// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unrealTopDownShooterGameMode.generated.h"

UCLASS(minimalapi)
class AunrealTopDownShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AunrealTopDownShooterGameMode();

	void PlayerCharacterDead();
};



